# Trànsit
Bot no oficial que obté les dades via RSS de les incidències viàries en temps real a Catalunya des del portal de dades obertes de la Generalitat i les publica en un servidor Mastodon.
Exemple a [mastodont.cat](https://mastodont.cat/@transit).

### Dependències

-   **Python 3**
-   Compte del bot a Mastodon
-   Servidor Postgresql

### Com instal·lar Trànsit:

1. Clona el repositori: `git clone https://codeberg.org/spla/transit.git <directori>`  

2. Entra amb `cd` al directori `<directori>` i crea l'Entorn Virtual de Python: `python3.x -m venv .`  
 
3. Activa l'Entorn Virtual de Python: `source bin/activate`  
  
4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python incidencies.py` per a aconseguir els tokens d'accés del compte del bot de  Mastodon i crear la base de dades.  

6. Fes servir el teu programador de tasques preferit (per exemple crontab) per a que `python incidencies.py` s'executi cada minut.  




