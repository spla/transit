from app.libraries.setup import Setup
from app.libraries.database import Database
from mastodon import Mastodon
from datetime import datetime, date
import re
import time
import sys
import os
import requests
from io import BytesIO
import feedparser
import emojis
import pdb

def check():

    try:

        resp = requests.get(f'http://www.gencat.cat/transit/opendata/incidenciesRSS.xml', timeout=3)

    except requests.exceptions.ConnectTimeout as conntimeout:

        pass

    content = BytesIO(resp.content)

    feed = feedparser.parse(content)

    return feed

if __name__ == '__main__':

    setup = Setup()

    db = Database()

    mastodon = Mastodon(
            access_token = setup.mastodon_app_token,
            api_base_url= setup.mastodon_hostname
            )

    feeds = check()

    i = 0
    while i < len(feeds):

        id = feeds.entries[i].id

        publish = db.published(id)

        if publish:

            title = feeds.entries[i].title

            summary = feeds.entries[i].summary

            post_text = f'{title}:\n\n{summary}'

            print(f'{post_text}')
            time.sleep(2)

            mastodon.status_post(post_text)

            db.write_feed(id)

        i += 1

